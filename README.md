# Heroku CLI [![AUR](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/Yash-Garg/heroku_cli/blob/master/LICENSE)

Heroku is a cloud platform as a service supporting several programming languages. This utility is an alternative for
installing heroku CLI commands on linux.

## How to use?

```bash
$ heroku (command)

$ COMMANDS
  access          manage user access to apps
  addons          tools and services for developing, extending, and operating your app
  apps            manage apps on Heroku
  auth            check 2fa status
  authorizations  OAuth authorizations
  autocomplete    display autocomplete installation instructions
  buildpacks      scripts used to compile apps
  certs           a topic for the ssl plugin
  ci              run an application test suite on Heroku
  clients         OAuth clients on the platform
  config          environment variables of apps
  container       Use containers to build and deploy Heroku apps
  domains         custom domains for apps
  drains          forward logs to syslog or HTTPS
  features        add/remove app features
  git             manage local git repository for app
  help            display help for heroku
  keys            add/remove account ssh keys
  labs            add/remove experimental features
  local           run heroku app locally
  logs            display recent log output
  maintenance     enable/disable access to app
  members         manage organization members
  notifications   display notifications
  orgs            manage organizations
  pg              manage postgresql databases
  pipelines       groups of apps that share the same codebase
  plugins         list installed plugins
  ps              Client tools for Heroku Exec
  psql            open a psql shell to the database
  redis           manage heroku redis instances
  regions         list available regions for deployment
  releases        display the releases for an app
  reviewapps      disposable apps built on GitHub pull requests
  run             run a one-off process inside a Heroku dyno
  sessions        OAuth sessions
  spaces          manage heroku private spaces
  status          status of the Heroku platform
  teams           manage teams
  update          update the Heroku CLI
  webhooks        setup HTTP notifications of app activity
```

## Installation
You can simply install heroku by following these instructions, make sure you have `git` and `curl` installed.

```bash
# Clone the repository.
$ git clone https://github.com/Yash-Garg/heroku_cli

# Navigate to cloned directory
$ cd heroku_cli

# Grant required permissions
$ chmod +x install.sh

# Install it for using the commands
$ ./install.sh or bash install.sh
```

There you are done you have successfully built and installed **heroku cli** on your machine

## Supported Platforms
- linux x86
- linux x86_64
- macOS

## Issue or feature request?

Please write about issues and feature request [here](https://github.com/Yash-Garg/heroku_cli/issues).
